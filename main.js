// hecka

const moment = require('moment-timezone')

const Masto = require('mastodon')
const secrets = require('./config/secrets.json')
const M = new Masto({
  access_token: secrets.mastodon.access_token
})

const getReminders = require('./lib/get_reminders.js')
const reminders = getReminders(`${__dirname}/reminders`)

reminders.forEach(value => {
  let now = moment().tz(value.tz).format('HHmm')

  // skip if now is not any of the defined times
  if (value.times.every(value => { return value.toString() !== now }))
    return

  let params = {
    status: value.message,
    visibility: value.visibility
  }

  M.post('statuses', params, (err, data, response) => {
    if (err)
      return console.error(err.message)

    return console.log('success')

  })

})
