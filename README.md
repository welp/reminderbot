# reminderbot

hi hi hello i’m supposed to remind you to do something

## what

this is a little mastodon bot to remind you and your friends to do the
things you sometimes forget to do

the [masto client](https://github.com/jhayley/node-mastodon) is based
on twit so you can make a twitterbot instead if you want

make a mastodon account for your bot then [create an
application](https://selfy.army/settings/applications) for it, and add
the access token to `config/secrets.json`

then you add reminders to `reminders/` in the yaml format:

```
---
tz: America/Los_Angeles
times:
  - 2200
message: '@catalina@selfy.army hey fucko go to bed'
visibility: private
```

then add a cron job to run once a minute:

```
*/1 * * * * /usr/bin/node /path/to/this/repo/main.js >> /place/for/logs/log.log 2>&1
```

i think that’s it?
