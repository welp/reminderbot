const fs = require('fs');
const yaml = require('js-yaml');

function getReminders(dir) {
  try {
    return mapDir(fs.readdirSync(dir));
  } catch (e) {
    console.error(e);
    return [];
  }
}

function mapDir(files) {
  return files.filter(value => {
    return value.substr(-5) === '.yaml' || value.substr(-4) === '.yml'
  }).map(value => {
    try {
      let yam = fs.readFileSync(`${__dirname}/../reminders/${value}`)
      return Object.assign({ dir: value }, yaml.safeLoad(yam))
    } catch (e) {
      console.error(e);
      return null
    }
  }).filter(value => {
    return value !== null
  })
}

module.exports = getReminders;
